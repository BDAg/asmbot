import requests
import json
from bs4 import BeautifulSoup


url_base = 'https://lista.mercadolivre.com.br/'
produto_nome = input('Qual produto deseja ? ')
response = requests.get(url_base + produto_nome)

site = BeautifulSoup(response.text, 'html.parser')

produtos = site.find_all('div', attrs={'class': 'andes-card andes-card--flat andes-card--default ui-search-result ui-search-result--core andes-card--padding-default'})

def importar_dados(dados):
    with open('dados_mercado_livre.json', 'a', encoding='UTF-8') as arquivo:
        json.dump(dados, arquivo, ensure_ascii=False, sort_keys=True, indent=4, separators=(',',':') )

menor_valor = []
aux = 0

for produto in produtos:

    titulo = produto.find('h2', attrs={'class':'ui-search-item__title'})
    link = produto.find('a', attrs={'class': 'ui-search-link'})
    moeda = produto.find('span', attrs={'class':'price-tag-symbol'})
    real = produto.find('span', attrs={'class':'price-tag-fraction'})
    centavo = produto.find('span', attrs={'class':'price-tag-cents'})

    if centavo:
        valor = moeda.text + real.text + ',' + centavo.text
    else:
        valor = moeda.text + real.text
    infos = {}
    infos['Anuncio'] = [
        {
            "Titulo": titulo.text,
            "Link": link['href'],
            "Valor": valor


        }
    ]
    aux = valor.replace("R$", "")
    aux = valor.replace(".", "")
    #aux = valor.replace(",", ".")

    #print(aux)
    #print(float(aux.replace("R$", "")))
    menor_valor.append((aux.replace("R$", "")))
    importar_dados(valor)


    #print(produto.prettify())
    print("Titulo:",titulo.text)
    print("Link do produto:", link['href'])
    if centavo:
        print(f'Valor: {moeda.text} {real.text},{centavo.text}')
    else:
        print(f'Valor: {moeda.text} {real.text}')
    print("\n\n")
print("Produto com menor valor: ",min(menor_valor))
print("Produto com maior valor: ",max(menor_valor))
